import pandas as pd

df_CA = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\CA.csv')
df_DE = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\DE.csv')
df_FR = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\FR.csv')
df_GB = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\GB.csv')
df_US = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\US.csv')


df_CA_revised = pd.DataFrame(df_CA, columns=['views','likes','dislikes', 'comment_count','tags'])
df_DE_revised = pd.DataFrame(df_DE, columns=['views','likes','dislikes', 'comment_count','tags'])
df_FR_revised = pd.DataFrame(df_FR, columns=['views','likes','dislikes', 'comment_count','tags'])
df_GB_revised = pd.DataFrame(df_GB, columns=['views','likes','dislikes', 'comment_count','tags'])
df_US_revised = pd.DataFrame(df_US, columns=['views','likes','dislikes', 'comment_count','tags'])

df_CA_revised = df_CA_revised.groupby(['tags'],as_index=False)[
        'views','likes','dislikes', 'comment_count'].max()
df_DE_revised = df_DE_revised.groupby(['tags'],as_index=False)[
        'views','likes','dislikes', 'comment_count'].max()
df_FR_revised = df_FR_revised.groupby(['tags'],as_index=False)[
        'views', 'likes','dislikes', 'comment_count'].max()
df_GB_revised = df_GB_revised.groupby(['tags'],as_index=False)[
        'views','likes','dislikes', 'comment_count'].max()
df_US_revised = df_US_revised.groupby(['tags'],as_index=False)[
        'views','likes','dislikes', 'comment_count'].max()

df_Total = pd.DataFrame()
df_Total = df_Total.append(df_CA_revised)
df_Total = df_Total.append(df_DE_revised)
df_Total = df_Total.append(df_FR_revised)
df_Total = df_Total.append(df_GB_revised)
df_Total = df_Total.append(df_US_revised)