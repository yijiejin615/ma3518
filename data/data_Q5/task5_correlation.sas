proc import datafile = "F:\MA3518 project\data_task5\CA_max.csv" out = data dbms=csv replace;
run;



PROC REG DATA=data;
  model views=likes dislikes likesdislikes/p;
  plot views*likes='+' p.*likes='*' /overlay;
  plot views*dislikes='+' p.*dislikes='*' /overlay;
RUN;

PROC CORR DATA=data;
	VAR views likes dislikes likesdislikes;
RUN;




