import pandas as pd

df_CA = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\CA_max.csv')
df_DE = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\DE_max.csv')
df_FR = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\FR_max.csv')
df_GB = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\GB_max.csv')
df_US = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\US_max.csv')


df_CA_1 = pd.DataFrame(df_CA,columns=['category_id',
                                      'views','likes','dislikes','comment_count'])
df_CA_1['video_count']=1
df_DE_1 = pd.DataFrame(df_DE,columns=['category_id',
                                      'views','likes','dislikes','comment_count'])
df_DE_1['video_count']=1
df_FR_1 = pd.DataFrame(df_FR,columns=['category_id',
                                      'views','likes','dislikes','comment_count'])
df_FR_1['video_count']=1
df_GB_1 = pd.DataFrame(df_GB,columns=['category_id',
                                      'views','likes','dislikes','comment_count'])
df_GB_1['video_count']=1
df_US_1 = pd.DataFrame(df_US,columns=['category_id',
                                     'views','likes','dislikes','comment_count'])
df_US_1['video_count']=1
df_CA_sum = df_CA_1.groupby(['category_id'],as_index=False)['views',
                           'likes','dislikes','comment_count','video_count'].sum()
del(df_CA_1)
df_DE_sum = df_DE_1.groupby(['category_id'],as_index=False)['views',
                           'likes','dislikes','comment_count','video_count'].sum()
del(df_DE_1)
df_FR_sum = df_FR_1.groupby(['category_id'],as_index=False)['views',
                           'likes','dislikes','comment_count','video_count'].sum()
del(df_FR_1)
df_GB_sum = df_GB_1.groupby(['category_id'],as_index=False)['views',
                           'likes','dislikes','comment_count','video_count'].sum()
del(df_GB_1)
df_US_sum = df_US_1.groupby(['category_id'],as_index=False)['views',
                           'likes','dislikes','comment_count','video_count'].sum()
del(df_US_1)

df_Total_sum = pd.DataFrame()
df_Total_sum = df_Total_sum.append(df_CA_sum)
df_Total_sum = df_Total_sum.append(df_DE_sum)
df_Total_sum = df_Total_sum.append(df_FR_sum)
df_Total_sum = df_Total_sum.append(df_GB_sum)
df_Total_sum = df_Total_sum.append(df_US_sum)
df_Total_sum = df_Total_sum.groupby(['category_id'],as_index=False)['views',
                           'likes','dislikes','comment_count','video_count'].sum()

df_CA_sum.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task3\\CA.csv')
df_DE_sum.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task3\\DE.csv')
df_FR_sum.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task3\\FR.csv')
df_GB_sum.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task3\\GB.csv')
df_US_sum.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task3\\US.csv')
df_Total_sum.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task3\\Total.csv')