import pandas as pd

df_Total_sum = pd.DataFrame()
df_Total_sum = df_Total_sum.append(df_CA_sum)
df_Total_sum = df_Total_sum.append(df_DE_sum)
df_Total_sum = df_Total_sum.append(df_FR_sum)
df_Total_sum = df_Total_sum.append(df_GB_sum)
df_Total_sum = df_Total_sum.append(df_US_sum)
df_Total_sum = df_Total_sum.groupby(['category_id'],as_index=False)['views',
                           'likes','dislikes','comment_count'].sum()