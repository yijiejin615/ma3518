proc import datafile = "E:\MA3518 project\data_task2\US_max.csv" out = Data dbms=csv replace;
run;


proc glm data=Data;
class title_length channel_title_length;
model views = title_length  channel_title_length  title_length*channel_title_length;
means  title_length  channel_title_length  title_length*channel_title_length;
run;

proc glm data=Data;
class title_length channel_title_length;
model likes = title_length  channel_title_length  title_length*channel_title_length;
means  title_length  channel_title_length  title_length*channel_title_length;
run;

proc glm data=Data;
class title_length channel_title_length;
model dislikes = title_length  channel_title_length  title_length*channel_title_length;
means  title_length  channel_title_length  title_length*channel_title_length;
run;
