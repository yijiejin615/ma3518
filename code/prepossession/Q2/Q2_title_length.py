import pandas as pd
import numpy as np

df_CA = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\CA.csv')
df_DE = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\DE.csv')
df_FR = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\FR.csv')
df_GB = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\GB.csv')
df_US = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\US.csv')

df_CA_1 = pd.DataFrame(df_CA, columns = ['title','channel_title', 
            'category_id', 'views','likes', 'dislikes', 'comment_count'])
df_CA_1['title_length'] = [len(i) for i in df_CA_1['title']]
df_CA_1['channel_title_length'] = [len(i) for i in df_CA_1['channel_title']]


df_DE_1 = pd.DataFrame(df_DE, columns = ['title','channel_title', 
            'category_id', 'views','likes', 'dislikes', 'comment_count'])
df_DE_1['title_length'] = [len(i) for i in df_DE_1['title']]
df_DE_1['channel_title_length'] = [len(i) for i in df_DE_1['channel_title']]


df_FR_1 = pd.DataFrame(df_FR, columns = ['title','channel_title', 
            'category_id', 'views','likes', 'dislikes', 'comment_count'])
df_FR_1['title_length'] = [len(i) for i in df_FR_1['title']]
df_FR_1['channel_title_length'] = [len(i) for i in df_FR_1['channel_title']]


df_GB_1 = pd.DataFrame(df_GB, columns = ['title','channel_title', 
            'category_id', 'views','likes', 'dislikes', 'comment_count'])
df_GB_1['title_length'] = [len(i) for i in df_GB_1['title']]
df_GB_1['channel_title_length'] = [len(i) for i in df_GB_1['channel_title']]


df_US_1 = pd.DataFrame(df_US, columns = ['title','channel_title', 
            'category_id', 'views','likes', 'dislikes', 'comment_count'])
df_US_1['title_length'] = [len(i) for i in df_US_1['title']]
df_US_1['channel_title_length'] = [len(i) for i in df_US_1['channel_title']]

df_Total_1 = pd.DataFrame()
df_Total_1 = df_Total_1.append(df_CA_1)
df_Total_1 = df_Total_1.append(df_DE_1)
df_Total_1 = df_Total_1.append(df_FR_1)
df_Total_1 = df_Total_1.append(df_GB_1)
df_Total_1 = df_Total_1.append(df_US_1)

#Do merge

df_CA_temp1 = df_CA_1.groupby(['title','channel_title'],as_index=False)['views','likes','dislikes','comment_count'].max()
df_CA_revised = pd.merge(df_CA_temp1,df_CA_1,how='inner',on=['title','channel_title'])
df_CA_revised = pd.DataFrame(df_CA_revised, columns=['title','channel_title','views_x',
   'likes_x','dislikes_x','comment_count_x','category_id','title_length','channel_title_length'])
df_CA_revised.columns = ['title','channel_title','views',
   'likes','dislikes','comment_count','category_id','title_length','channel_title_length']
df_CA_revised=df_CA_revised.drop_duplicates()
del(df_CA_temp1)

df_DE_temp1 = df_DE_1.groupby(['title','channel_title'],as_index=False)['views','likes','dislikes','comment_count'].max()
df_DE_revised = pd.merge(df_DE_temp1,df_DE_1,how='inner',on=['title','channel_title'])
df_DE_revised = pd.DataFrame(df_DE_revised, columns=['title','channel_title','views_x',
   'likes_x','dislikes_x','comment_count_x','category_id','title_length','channel_title_length'])
df_DE_revised.columns = ['title','channel_title','views',
   'likes','dislikes','comment_count','category_id','title_length','channel_title_length']
df_DE_revised=df_DE_revised.drop_duplicates()
del(df_DE_temp1)

df_FR_temp1 = df_FR_1.groupby(['title','channel_title'],as_index=False)['views','likes','dislikes','comment_count'].max()
df_FR_revised = pd.merge(df_FR_temp1,df_FR_1,how='inner',on=['title','channel_title'])
df_FR_revised = pd.DataFrame(df_FR_revised, columns=['title','channel_title','views_x',
   'likes_x','dislikes_x','comment_count_x','category_id','title_length','channel_title_length'])
df_FR_revised.columns = ['title','channel_title','views',
   'likes','dislikes','comment_count','category_id','title_length','channel_title_length']
df_FR_revised=df_FR_revised.drop_duplicates()
del(df_FR_temp1)

df_GB_temp1 = df_GB_1.groupby(['title','channel_title'],as_index=False)['views','likes','dislikes','comment_count'].max()
df_GB_revised = pd.merge(df_GB_temp1,df_GB_1,how='inner',on=['title','channel_title'])
df_GB_revised = pd.DataFrame(df_GB_revised, columns=['title','channel_title','views_x',
   'likes_x','dislikes_x','comment_count_x','category_id','title_length','channel_title_length'])
df_GB_revised.columns = ['title','channel_title','views',
   'likes','dislikes','comment_count','category_id','title_length','channel_title_length']
df_GB_revised=df_GB_revised.drop_duplicates()
del(df_GB_temp1)

df_US_temp1 = df_US_1.groupby(['title','channel_title'],as_index=False)['views','likes','dislikes','comment_count'].max()
df_US_revised = pd.merge(df_US_temp1,df_US_1,how='inner',on=['title','channel_title'])
df_US_revised = pd.DataFrame(df_US_revised, columns=['title','channel_title','views_x',
   'likes_x','dislikes_x','comment_count_x','category_id','title_length','channel_title_length'])
df_US_revised.columns = ['title','channel_title','views',
   'likes','dislikes','comment_count','category_id','title_length','channel_title_length']
df_US_revised=df_US_revised.drop_duplicates()
del(df_US_temp1)

df_Total_temp1 = df_Total_1.groupby(['title','channel_title'],as_index=False)['views','likes','dislikes','comment_count'].max()
df_Total_revised = pd.merge(df_Total_temp1,df_Total_1,how='inner',on=['title','channel_title'])
df_Total_revised = pd.DataFrame(df_Total_revised, columns=['title','channel_title','views_x',
   'likes_x','dislikes_x','comment_count_x','category_id','title_length','channel_title_length'])
df_Total_revised.columns = ['title','channel_title','views',
   'likes','dislikes','comment_count','category_id','title_length','channel_title_length']
df_Total_revised=df_Total_revised.drop_duplicates()
del(df_Total_temp1)

'''
df_CA_1.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\CA.csv')
df_DE_1.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\DE.csv')
df_FR_1.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\FR.csv')
df_GB_1.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\GB.csv')
df_US_1.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\US.csv')
df_Total_1.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\Total.csv')
'''
'''
df_CA_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\CA_max.csv')
df_DE_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\DE_max.csv')
df_FR_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\FR_max.csv')
df_GB_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\GB_max.csv')
df_US_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\US_max.csv')
df_Total_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task2\\Total_max.csv')
'''