proc import datafile = "E:\MA3518 project\data_task2\CA_max.csv" out = Data dbms=csv replace;
run;

proc glm data=CA;
class title_length channel_title_length;
model views = title_length  channel_title_length  title_length*channel_title_length;
means  title_length  channel_title_length  title_length*channel_title_length;
run;

proc glm data=CA;
class title_length channel_title_length;
model likes = title_length  channel_title_length  title_length*channel_title_length;
means  title_length  channel_title_length  title_length*channel_title_length;
run;

proc glm data=CA;
class title_length channel_title_length;
model dislikes = title_length  channel_title_length  title_length*channel_title_length;
means  title_length  channel_title_length  title_length*channel_title_length;
run;
