import pandas as pd

df_CA = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\CA.csv')
df_DE = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\DE.csv')
df_FR = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\FR.csv')
df_GB = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\GB.csv')
df_US = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_start\\US.csv')

df_CA_revised = pd.DataFrame(df_CA, columns=['video_id','category_id','publish_time', 'trending_date'])
df_DE_revised = pd.DataFrame(df_DE, columns=['video_id','category_id','publish_time', 'trending_date'])
df_FR_revised = pd.DataFrame(df_FR, columns=['video_id','category_id','publish_time', 'trending_date'])
df_GB_revised = pd.DataFrame(df_GB, columns=['video_id','category_id','publish_time', 'trending_date'])
df_US_revised = pd.DataFrame(df_US, columns=['video_id','category_id','publish_time', 'trending_date'])

df_Total=pd.DataFrame()
df_Total=df_Total.append(df_CA_revised)
df_Total=df_Total.append(df_DE_revised)
df_Total=df_Total.append(df_FR_revised)
df_Total=df_Total.append(df_GB_revised)
df_Total=df_Total.append(df_US_revised)


df_CA_noDup = df_CA.groupby(['video_id','category_id'],as_index=False)[
        'trending_date','publish_time'].min()
df_DE_noDup = df_DE.groupby(['video_id','category_id'],as_index=False)[
        'trending_date','publish_time'].min()
df_FR_noDup = df_FR.groupby(['video_id','category_id'],as_index=False)[
        'trending_date','publish_time'].min()
df_GB_noDup = df_GB.groupby(['video_id','category_id'],as_index=False)[
        'trending_date','publish_time'].min()
df_US_noDup = df_US.groupby(['video_id','category_id'],as_index=False)[
        'trending_date','publish_time'].min()
df_Total_noDup = df_Total.groupby(['video_id','category_id'],as_index=False)[
        'trending_date','publish_time'].min()

'''
df_CA_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\CA.csv')
df_DE_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\DE.csv')
df_FR_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\FR.csv')
df_GB_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\GB.csv')
df_US_revised.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\US.csv')
df_Total.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\Total.csv')

df_CA_noDup.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\CA_noDup.csv')
df_DE_noDup.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\DE_noDup.csv')
df_FR_noDup.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\FR_noDup.csv')
df_GB_noDup.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\GB_noDup.csv')
df_US_noDup.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\US_noDup.csv')
df_Total_noDup.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\Total_noDup.csv')
'''