import pandas as pd
from datetime import date

df_CA = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\CA_noDup.csv')
df_DE = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\DE_noDup.csv')
df_FR = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\FR_noDup.csv')
df_GB = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\GB_noDup.csv')
df_US = pd.read_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\US_noDup.csv')


df_CA['trending_date']=[date(int(i[0:2])+2000, int(i[6:8]),int(i[3:5])) for i in df_CA['trending_date']]
df_CA['publish_time']=[date(int(i[0:4]), int(i[5:7]),int(i[8:10])) for i in df_CA['publish_time']]
df_CA['delta'] = df_CA['trending_date'] - df_CA['publish_time']
df_CA['delta'] = df_CA['delta'].dt.days
df_CA=pd.DataFrame(df_CA,columns=['video_id',
                                  'category_id','publish_time','trending_date','delta'])

df_DE['trending_date']=[date(int(i[0:2])+2000, int(i[6:8]),int(i[3:5])) for i in df_DE['trending_date']]
df_DE['publish_time']=[date(int(i[0:4]), int(i[5:7]),int(i[8:10])) for i in df_DE['publish_time']]
df_DE['delta'] = df_DE['trending_date'] - df_DE['publish_time']
df_DE['delta'] = df_DE['delta'].dt.days
df_DE=pd.DataFrame(df_DE,columns=['video_id',
                                  'category_id','publish_time','trending_date','delta'])

df_FR['trending_date']=[date(int(i[0:2])+2000, int(i[6:8]),int(i[3:5])) for i in df_FR['trending_date']]
df_FR['publish_time']=[date(int(i[0:4]), int(i[5:7]),int(i[8:10])) for i in df_FR['publish_time']]
df_FR['delta'] = df_FR['trending_date'] - df_FR['publish_time']
df_FR['delta'] = df_FR['delta'].dt.days
df_FR=pd.DataFrame(df_FR,columns=['video_id',
                                  'category_id','publish_time','trending_date','delta'])

df_GB['trending_date']=[date(int(i[0:2])+2000, int(i[6:8]),int(i[3:5])) for i in df_GB['trending_date']]
df_GB['publish_time']=[date(int(i[0:4]), int(i[5:7]),int(i[8:10])) for i in df_GB['publish_time']]
df_GB['delta'] = df_GB['trending_date'] - df_GB['publish_time']
df_GB['delta'] = df_GB['delta'].dt.days
df_GB=pd.DataFrame(df_GB,columns=['video_id',
                                  'category_id','publish_time','trending_date','delta'])

df_US['trending_date']=[date(int(i[0:2])+2000, int(i[6:8]),int(i[3:5])) for i in df_US['trending_date']]
df_US['publish_time']=[date(int(i[0:4]), int(i[5:7]),int(i[8:10])) for i in df_US['publish_time']]
df_US['delta'] = df_US['trending_date'] - df_US['publish_time']
df_US['delta'] = df_US['delta'].dt.days
df_US=pd.DataFrame(df_US,columns=['video_id',
                                  'category_id','publish_time','trending_date','delta'])

df_Total=pd.DataFrame()
df_Total=df_Total.append(df_CA)
df_Total=df_Total.append(df_DE)
df_Total=df_Total.append(df_FR)
df_Total=df_Total.append(df_GB)
df_Total=df_Total.append(df_US)

df_CA.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\CA_delta.csv')
df_DE.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\DE_delta.csv')
df_FR.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\FR_delta.csv')
df_GB.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\GB_delta.csv')
df_US.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\US_delta.csv')
df_Total.to_csv('C:\\Users\\lenovo\\Desktop\\MA3518 project\\data_task1\\Total_delta.csv')