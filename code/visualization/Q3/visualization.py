import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
from pyecharts import Bar, Grid, Scatter, WordCloud, Page

# views	 likes	 dislikes	 comment_count	 video_count

def id_to_name(category_id):
    # change the category id number into the real name
    all_id = [1, 2, 10, 15, 17, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 43, 44]
    all_name = {1: "Film & Animation", 
                2: "Autos & Vehicles", 
                10: "Music",
                15: "Pets & Animals",
                17: "Sports",
                18: "Short Movies",
                19: "Travel & Events",
                20: "Gaming",
                21: "Videoblogging",
                22: "People & Blogs",
                23: "Comedy",
                24: "Entertainment",
                25: "News & Politics",
                26: "Howto & Style",
                27: "Education",
                28: "Science & Technology",
                29: "Nonprofits & Activism",
                30: "Movies",
                43: "Shows",
                44: "Trailers"
                }
    name = []
    for i in range(len(category_id)):
        if category_id[i] in all_id:
            name.append(all_name[category_id[i]])
    return name

def mean(region):
    df = pd.read_csv(region+".csv")
    length = len(df["views"].values)
    category_id = id_to_name(df["category_id"].values)
    views = df["views"].values
    likes = df["likes"].values
    dislikes = df["dislikes"].values
    comment_count = df["comment_count"].values
    video_count = df["video_count"].values

    views_mean = [views[i]/video_count[i] for i in range(length)]
    likes_mean = [likes[i]/video_count[i] for i in range(length)]
    dislikes_mean = [dislikes[i]/video_count[i] for i in range(length)]
    comments_mean = [comment_count[i]/video_count[i] for i in range(length)]

    df["views_mean"] = views_mean
    df["likes_mean"] = likes_mean
    df["dislikes_mean"] = dislikes_mean
    df["comments_mean"] = comments_mean

    toBar(category_id, views_mean, likes_mean, dislikes_mean, comments_mean, region)
    toScatter(category_id, views_mean, likes_mean, dislikes_mean, comments_mean, region)
    toWordcloud(category_id, views_mean, likes_mean, dislikes_mean, comments_mean, region)
    df.to_csv(region+"_mean.csv")

def toBar(category_id, views_mean, likes_mean, dislikes_mean, comments_mean, region):
    grid = Grid()
    bar = Bar(region+" Mean vs Category")
    bar.add("views", category_id, views_mean, is_datazoom_show=True, xaxis_interval=0, xaxis_rotate=30)
    bar.add("likes", category_id, likes_mean, is_datazoom_show=True, xaxis_interval=0, xaxis_rotate=30)
    bar.add("dislikes", category_id, dislikes_mean, is_datazoom_show=True, xaxis_interval=0, xaxis_rotate=30)
    bar.add("comments", category_id, comments_mean, is_datazoom_show=True, xaxis_interval=0, xaxis_rotate=30)
    grid.add(bar, grid_bottom="25%")
    grid.render(region+"_bar.html")

def toScatter(category_id, views_mean, likes_mean, dislikes_mean, comments_mean, region):
    # draw the scatter maps in one page 
    page = Page()
    grid = Grid()
    grid1 = Grid()
    grid2 = Grid()
    grid3 = Grid()
    grid4 = Grid()
    scatter1 = Scatter(region+" Mean vs Category")
    scatter2 = Scatter(region+" Mean vs Category")
    scatter3 = Scatter(region+" Mean vs Category")
    scatter4 = Scatter(region+" Mean vs Category")
    scatter1.add(
        "views", 
        category_id, 
        views_mean,
        is_visualmap=True,
        visual_type="size",
        xaxis_type="category",
        xaxis_rotate=30,
        is_more_utils = True,
        is_yaxis_boundarygap = True,
        visual_pos = 1000,
        visual_range=[min(views_mean), max(views_mean)]
    )
    scatter2.add(
        "likes", 
        category_id, 
        likes_mean,
        is_visualmap=True,
        visual_type="size",
        xaxis_type="category",
        xaxis_rotate=30,
        is_more_utils = True,
        visual_pos = 1000,
        visual_range=[min(likes_mean), max(likes_mean)]
    )
    
    scatter3.add(
        "dislikes", 
        category_id, 
        dislikes_mean,
        is_visualmap=True,
        visual_type="size",
        xaxis_type="category",
        xaxis_rotate=30,
        is_more_utils = True,
        visual_pos = 1000,
        visual_range=[min(dislikes_mean), max(dislikes_mean)]
    )
    scatter4.add(
        "comments", 
        category_id, 
        comments_mean,
        is_visualmap=True,
        visual_type="size",
        xaxis_type="category",
        xaxis_rotate=30,
        is_more_utils = True,
        visual_pos = 1000,
        visual_range=[min(comments_mean), max(comments_mean)]
    )

    grid1.add(scatter1, grid_bottom="25%")
    grid2.add(scatter2, grid_bottom="25%")
    grid3.add(scatter3, grid_bottom="25%")
    grid4.add(scatter4, grid_bottom="25%")
    page.add(grid1)
    page.add(grid2)
    page.add(grid3)
    page.add(grid4)
    page.render(region+"_scatter.html")
   

def toWordcloud(category_id, views_mean, likes_mean, dislikes_mean, comments_mean, region):
    page = Page()
    wordcloud1 = WordCloud(width=1300, height=620)
    wordcloud2 = WordCloud(width=1300, height=620)
    wordcloud3 = WordCloud(width=1300, height=620)
    wordcloud4 = WordCloud(width=1300, height=620)
    wordcloud1.add("views", category_id, views_mean, word_size_range=[20,100],shape='diamond')
    wordcloud2.add("likes", category_id, likes_mean, word_size_range=[20,100],shape='diamond')
    wordcloud3.add("dislikes", category_id, dislikes_mean, word_size_range=[40,120],shape='diamond')
    wordcloud4.add("comments", category_id, comments_mean, word_size_range=[20,100],shape='diamond')
    page.add(wordcloud1)
    page.add(wordcloud2)
    page.add(wordcloud3)
    page.add(wordcloud4)
    page.render(region+"_wordcloud.html")

if __name__ == "__main__":
    mean("CA")
    mean("DE")
    mean("FR")
    mean("GB")
    mean("US")
    mean("Total")