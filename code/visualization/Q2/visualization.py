import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt
from pyecharts import Scatter

def toMatrix(df):
    # Convert dataframe series into numpy array
    title_length = CA_max_df["title_length"].as_matrix()
    channel_length = CA_max_df["channel_title_length"].as_matrix()
    likes = CA_max_df["likes"].as_matrix()
    dislikes = CA_max_df["dislikes"].as_matrix()
    views = CA_max_df["views"].as_matrix()
    return title_length, channel_length, likes, dislikes, views

def toList(df):
    title_length, channel_length, likes, dislikes, views = toMatrix(df)
    title_length_list = list(title_length)
    channel_length_list = list(channel_length)
    likes_list = list(likes)
    dislikes_list = list(dislikes)
    views_list = list(views)
    return title_length_list, channel_length_list, likes_list, dislikes_list, views_list

def toScatter(region, title_length_list, channel_length_list, likes_list, dislikes_list, views_list):
    # Use scatter to show the relationship between the number of likes, dislikes, views and lengths
    scatter1 = Scatter("likes vs title length")
    scatter1.add("likes", likes_list, title_length_list, is_visualmap = True, visual_range = [0,100], is_datazoom_show = True)
    scatter1.render(region+"_likes_vs_title_length.html")

    scatter2 = Scatter("likes vs channel length")
    scatter2.add("likes", likes_list,channel_length_list, is_visualmap = True, visual_range = [0,60], is_datazoom_show = True)
    scatter2.render(region+"_likes_vs_channel_length.html")

    scatter3 = Scatter("dislikes vs title length")
    scatter3.add("dislikes", dislikes_list, title_length_list, is_visualmap = True, visual_range = [0,100], is_datazoom_show = True)
    scatter3.render(region+"_dislikes_vs_title_length.html")

    scatter4 = Scatter("dislikes vs channel length")
    scatter4.add("dislikes", dislikes_list, channel_length_list, is_visualmap = True, visual_range = [0,60], is_datazoom_show = True)
    scatter4.render(region+"_dislikes_vs_channel_length.html")

    scatter5 = Scatter("views vs title length")
    scatter5.add("views", views_list, title_length_list, is_visualmap = True, visual_range = [0,100], is_datazoom_show = True)
    scatter5.render(region+"_views_vs_title_length.html")

    scatter6 = Scatter("views vs channel length")
    scatter6.add("views", views_list, channel_length_list, is_visualmap = True, visual_range = [0,60], is_datazoom_show = True)
    scatter6.render(region+"_views_vs_channel_length.html")

    # In scatter 7 and 8, likes, dislikes, views are put in the same scatter map
    # scatter 7 is about title length
    # scatter 8 is about channel length
    scatter7 = Scatter("_likes, dislikes, views vs title length")
    scatter7.add(
        "likes",
        title_length_list,
        likes_list,
        is_visualmap = True, 
        visual_range = [0,max(views_list)], 
        is_datazoom_show = True,
        datazoom_orient = "vertical",
        tooltip_trigger = "item"
    )
    scatter7.add(
        "dislikes", 
        title_length_list, 
        dislikes_list, 
        is_visualmap = True, 
        visual_range = [0,max(views_list)], 
        is_datazoom_show = True,
        datazoom_orient = "vertical",
        tooltip_trigger = "item"
    )
    scatter7.add(
        "views", 
        title_length_list,
        views_list, 
        is_visualmap = True, 
        visual_range = [0,max(views_list)], 
        is_datazoom_show = True,
        datazoom_orient = "vertical",
        tooltip_trigger = "item"
    )
    scatter7.render(region+"_likes_dislikes_views_title.html")

    scatter8 = Scatter("likes, dislikes, views vs channel length")
    scatter8.add(
        "likes",
        channel_length_list, 
        likes_list, 
        is_visualmap = True, 
        visual_range = [0,max(views_list)], 
        is_datazoom_show = True,
        datazoom_orient = "vertical",
        tooltip_trigger = "item"
    )
    scatter8.add(
        "dislikes", 
        channel_length_list,
        dislikes_list,
        is_visualmap = True, 
        visual_range = [0,max(views_list)], 
        is_datazoom_show = True,
        datazoom_orient = "vertical",
        tooltip_trigger = "item"
    )
    scatter8.add(
        "views", 
        channel_length_list,
        views_list, 
        is_visualmap = True, 
        visual_range = [0,max(views_list)], 
        is_datazoom_show = True,
        datazoom_orient = "vertical",
        tooltip_trigger = "item"
    )
    scatter8.render(region+"_likes_dislikes_views_channel.html")


if __name__ == "__main__":
    # CA GB FR DE US
    CA_max_df = pd.read_csv("/home/yijiejin/Documents/MA3518/ma3518/data_task2/CA_max.csv")
    GB_max_df = pd.read_csv("/home/yijiejin/Documents/MA3518/ma3518/data_task2/GB_max.csv")
    FR_max_df = pd.read_csv("/home/yijiejin/Documents/MA3518/ma3518/data_task2/FR_max.csv")
    DE_max_df = pd.read_csv("/home/yijiejin/Documents/MA3518/ma3518/data_task2/DE_max.csv")
    US_max_df = pd.read_csv("/home/yijiejin/Documents/MA3518/ma3518/data_task2/US_max.csv")
    Total_max_df = pd.read_csv("/home/yijiejin/Documents/MA3518/ma3518/data_task2/Total_max.csv")
    CA_title_length_list, CA_channel_length_list, CA_likes_list, CA_dislikes_list, CA_views_list = toList(CA_max_df)
    GB_title_length_list, GB_channel_length_list, GB_likes_list, GB_dislikes_list, GB_views_list = toList(GB_max_df)
    FR_title_length_list, FR_channel_length_list, FR_likes_list, FR_dislikes_list, FR_views_list = toList(FR_max_df)
    DE_title_length_list, DE_channel_length_list, DE_likes_list, DE_dislikes_list, DE_views_list = toList(DE_max_df)
    US_title_length_list, US_channel_length_list, US_likes_list, US_dislikes_list, US_views_list = toList(US_max_df)
    Total_title_length_list, Total_channel_length_list, Total_likes_list, Total_dislikes_list, Total_views_list = toList(Total_max_df)
    toScatter("CA", CA_title_length_list, CA_channel_length_list, CA_likes_list, CA_dislikes_list, CA_views_list)
    toScatter("GB", GB_title_length_list, GB_channel_length_list, GB_likes_list, GB_dislikes_list, GB_views_list)
    toScatter("FR", FR_title_length_list, FR_channel_length_list, FR_likes_list, FR_dislikes_list, FR_views_list)
    toScatter("DE", DE_title_length_list, DE_channel_length_list, DE_likes_list, DE_dislikes_list, DE_views_list)
    toScatter("US", US_title_length_list, US_channel_length_list, US_likes_list, US_dislikes_list, US_views_list)
    toScatter("Total", Total_title_length_list, Total_channel_length_list, Total_likes_list, Total_dislikes_list, Total_views_list)