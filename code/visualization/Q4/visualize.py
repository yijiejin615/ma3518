import pandas as pd 
import numpy as np 
from pyecharts import WordCloud

def toWordcloud(words, count):
    wordcloud = WordCloud(width=1300, height=620)
    wordcloud.add("hot tags", words, count, word_size_range=[20,100],shape='diamond')
    wordcloud.render("hot_tags_wordcloud.html")

if __name__ == "__main__":
    df = pd.read_csv("/home/yijiejin/Documents/MA3518/ma3518/JYJ/task4/Total_hot_word.csv")
    words = df["word"].values
    count = df["count"].values
    words = [words[i].replace("\"", "") for i in range(len(count))]
    toWordcloud(words, count)
