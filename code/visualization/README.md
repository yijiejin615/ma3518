# Visualization of task2
* `visualize.py` contains the visualization of task2 about only data in Canada.
  It is implemented with pyecharts. The html files are the output of the code.
* Task2
  * I have drawed the scatter map of views, likes, dislikes with respect to title length and channel length
    respectively separately and jointly (in case we need to separate views, likes and dislikes) in the five regions and total. 
    <br/>
    For example,<br/> 
    * `CA_likes_dislikes_views_title.html`: the likes, dislikes, views with respect to title length in Canada.
    * `CA_likes_dislikes_views_channel.html`: the likes, dislikes, views with respect to channel length in Canada.
     
