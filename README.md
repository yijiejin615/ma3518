# MA3518 Project
## Topic: If you are a YouTuber, how do you make your video popular?

The project aims at exploring what factors make a video on YouTube popular and thus guides a fresh YouTuber to make popular videos. We firstly analyzed the how much time a video usually needs to become popular by comparing the difference between the publishing time and trending time. In addition, we applied two-way ANOVA to test whether there is a relationship between the views a video gains,  the title length and the channel length. We found that channels with moderate length have most views while the title length does not influence much. Furthermore, since different topic may have different attractions, we also explore the influence of topics by analyzing the statistics of categories and tags. As the results suggest, a YouTuber could choose topics such as music and comedy in order to attract more audiences. Besides, in the previous observation, we found some videos have both high views and dislikes, which implies that a controversial video may also be popular. Thus, we calculate the correlations between a video’s likes, dislikes, views, and comments respectively. 

To analyze the factors of popularity of videos on YouTube, we proposed five questions to solve.

1.	How long does it take for a video to become popular?
2.	Do the lengths of the title and channel title influence the popularity of a video?
3.	What categories of videos gain the most views?
4.	What are the most popular topics nowadays?
5.	What are the correlations between views, likes, dislikes and comments?

-------
## Description of the files:
* `code`: contains all the codes we use (including code for data preposessiong, statistical tests and visualization of the results). Besides, the visualization results are also in the folder.
* `data`: contains all the data (including the original data and some data after our prepossession).


